

require.config({ paths: { vs: '../node_modules/monaco-editor/min/vs' } });


require(['vs/editor/editor.main'], function () {
    const editor = monaco.editor.create(document.getElementById('container'), {
        value: ['#include <iostream>', 'int main(int argc,char**argv){','\treturn 0;', '}'].join('\n'),
        language: 'cpp',
        automaticLayout: true,
        minimap: { enabled: false },
        /* fixedOverflowWidgets: true,
        minimap: { enabled: false },

        scrollBeyondLastLine: false*/
        //scrollBeyondLastLine: false,
    });

});




